package com.example.WebApp.Controller;


import com.example.WebApp.Entity.Proizvod;
import com.example.WebApp.Service.KorisnikService;
import com.example.WebApp.Service.ProizvodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
@RestController
@RequestMapping("/proizvodi")
public class ProizvodController {

        @Autowired
        private ProizvodService proizvodService;

        @RequestMapping(method = RequestMethod.GET)
        public Collection<Proizvod> getKorisnici(){
            return this.proizvodService.getProizvodi();
        }

        @RequestMapping(value = "/{id}" , method = RequestMethod.GET)
        public Proizvod getProizvodById(@PathVariable("id") int id){
            return proizvodService.getProizvodById(id);
        }

    }

