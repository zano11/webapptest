package com.example.WebApp.Controller;

import com.example.WebApp.Entity.Korisnik;
import com.example.WebApp.Service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/korisnici")
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Korisnik> getKorisnici(){
        return this.korisnikService.getKorisnici();
    }

    @RequestMapping(value = "/{id}" , method = RequestMethod.GET)
    public Korisnik getKorisnikById(@PathVariable("id") int id){
        return korisnikService.getKorisnikById(id);
    }

    }

