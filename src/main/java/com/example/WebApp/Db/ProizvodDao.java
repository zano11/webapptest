package com.example.WebApp.Db;

import com.example.WebApp.Entity.Proizvod;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
@Repository
public class ProizvodDao {

    private static Map<Integer,Proizvod> proizvodi;

    static {

        proizvodi = new HashMap<Integer, Proizvod>(){
            {
                put(1, new Proizvod("torta", 350 , "Torti" , "20/01/2018"));
                put(2, new Proizvod("bbb", 250 , "bbb" , "15/01/2017"));


            }
        };
    }

    public Collection<Proizvod> getProizvod(){
        return this.proizvodi.values();
    }

    public Proizvod getProizvodById(int id){
        return  this.proizvodi.get(id);
    }
}


