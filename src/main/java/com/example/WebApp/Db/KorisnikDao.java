package com.example.WebApp.Db;

import com.example.WebApp.Entity.Korisnik;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class KorisnikDao {

    private static Map<Integer,Korisnik> korisnici;

    static {

        korisnici = new HashMap<Integer, Korisnik>(){
            {
                put(1 ,new Korisnik(1, "aaa@gmail.com", "aaaa", "aaaa", "071967432","Bitola"));
                put(2 ,new Korisnik(2, "bbb@gmail.com", "bbbb", "bbbb", "0712273432","Bitola"));
                put(3 ,new Korisnik(3, "ccc@gmail.com", "cccc", "cccc", "071222222","Skopje"));
                put(4 ,new Korisnik(4, "aadas@gmail.com", "asda", "sadascccc", "0712422222","Resen"));

            }
        };
    }

    public Collection<Korisnik> getKorisnici(){
        return this.korisnici.values();
    }

    public Korisnik getKorisnikById(int id){
      return  this.korisnici.get(id);
    }
}
