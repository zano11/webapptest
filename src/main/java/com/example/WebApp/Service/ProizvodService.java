package com.example.WebApp.Service;

import com.example.WebApp.Db.ProizvodDao;
import com.example.WebApp.Entity.Proizvod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ProizvodService {

    @Autowired
    public ProizvodDao proizvodDao;

    public Collection<Proizvod> getProizvodi() {
        return this.proizvodDao.getProizvod();
    }

    public Proizvod getProizvodById(int id){
        return  this.proizvodDao.getProizvodById(id);
    }


}
