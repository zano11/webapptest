package com.example.WebApp.Service;

import com.example.WebApp.Db.KorisnikDao;
import com.example.WebApp.Entity.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class KorisnikService {

    @Autowired
    private KorisnikDao korisnikDao;


    public Collection<Korisnik> getKorisnici(){
        return this.korisnikDao.getKorisnici();
    }

    public Korisnik getKorisnikById(int id){
        return  this.korisnikDao.getKorisnikById(id);
    }
}
